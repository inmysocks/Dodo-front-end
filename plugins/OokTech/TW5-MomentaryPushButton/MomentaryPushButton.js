/*\
title: $:/plugins/OokTech/PushButton/PushButton.js
type: application/javascript
module-type: widget

Button widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var MomentaryButtonWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
MomentaryButtonWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
MomentaryButtonWidget.prototype.render = function(parent,nextSibling) {
	var self = this;
	// Remember parent
	this.parentDomNode = parent;
	// Compute attributes and execute state
	this.computeAttributes();
	this.execute();
	// Create element
	var tag = "button";
	if(this.buttonTag && $tw.config.htmlUnsafeElements.indexOf(this.buttonTag) === -1) {
		tag = this.buttonTag;
	}
	var domNode = this.document.createElement(tag);
	// Assign classes
	var classes = this["class"].split(" ") || [],
		isPoppedUp = this.popup && this.isPoppedUp();
	if(this.selectedClass) {
		if(this.set && this.setTo && this.isSelected()) {
			$tw.utils.pushTop(classes,this.selectedClass.split(" "));
		}
		if(isPoppedUp) {
			$tw.utils.pushTop(classes,this.selectedClass.split(" "));
		}
	}
	if(isPoppedUp) {
		$tw.utils.pushTop(classes,"tc-popup-handle");
	}
	domNode.className = classes.join(" ");
	// Assign other attributes
	if(this.style) {
		domNode.setAttribute("style",this.style);
	}
	if(this.tooltip) {
		domNode.setAttribute("title",this.tooltip);
	}
	if(this["aria-label"]) {
		domNode.setAttribute("aria-label",this["aria-label"]);
	}
	// Add a mousedown event handler
	domNode.addEventListener("mousedown",function (event) {
		var handled = false;
		self.enabled = true;
		if(self.clickActions) {
			self.invokeActionString(self.clickActions,self,event);
		}
		if(handled) {
			event.preventDefault();
			event.stopPropagation();
		}
		return handled;
	},false);
	domNode.addEventListener("mouseup",function (event) {
		var handled = false;
		if(self.releaseActions) {
			self.invokeActionString(self.releaseActions,self,event);
		}
		if(handled) {
			event.preventDefault();
			event.stopPropagation();
		}
		self.enabled = false;
		return handled;
	},false);
	domNode.addEventListener("mouseout",function (event) {
		//If the button is held down than do the release action when the mouse leaves but only act if the button is being heald down when the mouse leaves
		if(self.enabled) {
			var handled = false;
			if(self.releaseActions) {
				self.invokeActionString(self.releaseActions,self,event);
			}
			if(handled) {
				event.preventDefault();
				event.stopPropagation();
			}
			self.enabled = false;
			return handled;
		}
	},false);
	// Insert element
	parent.insertBefore(domNode,nextSibling);
	this.renderChildren(domNode,null);
	this.domNodes.push(domNode);
};

/*
We don't allow actions to propagate because we trigger actions ourselves
*/
MomentaryButtonWidget.prototype.allowActionPropagation = function() {
	return false;
};

MomentaryButtonWidget.prototype.getBoundingClientRect = function() {
	return this.domNodes[0].getBoundingClientRect();
};

/*
Compute the internal state of the widget
*/
MomentaryButtonWidget.prototype.execute = function() {
	// Get attributes
	this.clickActions = this.getAttribute("clickActions");
	this.releaseActions = this.getAttribute("releaseActions");
	this["class"] = this.getAttribute("class","");
	this["aria-label"] = this.getAttribute("aria-label");
	this.tooltip = this.getAttribute("tooltip");
	this.style = this.getAttribute("style");
	this.selectedClass = this.getAttribute("selectedClass");
	this.defaultSetValue = this.getAttribute("default","");
	this.buttonTag = this.getAttribute("tag");
	// Make child widgets
	this.makeChildWidgets();
};

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
MomentaryButtonWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	//if(changedAttributes.to || changedAttributes.message || changedAttributes.param || changedAttributes.set || changedAttributes.setTo || changedAttributes.popup || changedAttributes.hover || changedAttributes["class"] || changedAttributes.selectedClass || changedAttributes.style || (this.set && changedTiddlers[this.set]) || (this.popup && changedTiddlers[this.popup])) {
	if (Object.keys(changedAttributes).length) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

exports.mbutton = MomentaryButtonWidget;

})();
