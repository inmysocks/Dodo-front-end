# TW5-GetData
A simple task list for tiddlywiki (https://github.com/Jermolene/TiddlyWiki5)

You can see an example on the demo site.

Demo Site: http://ooktech.com/TiddlyWiki/MomentaryPushButton/

Git Repo: https://github.com/OokTech/TW5-MomentaryPushButton
