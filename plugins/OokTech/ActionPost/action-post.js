/*\
title: $:/plugins/OokTech/action-post.js
type: application/javascript
module-type: widget

Action widget to send an XMLhttprequest with POST form data.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var ActionPost = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
ActionPost.prototype = new Widget();

/*
Render this widget into the DOM
*/
ActionPost.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
ActionPost.prototype.execute = function() {
	this.formURL = this.getAttribute("$url");
	this.formTiddler = this.getAttribute("$tiddler");
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
ActionPost.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(changedAttributes["$tiddler"] || changedAttributes["$url"]) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
ActionPost.prototype.invokeAction = function(triggeringWidget,event) {
	if (this.formTiddler && this.formURL) {
		var formData = "";
		var tiddler = this.wiki.getTiddler(this.formTiddler);
		var parsedTiddler = JSON.parse(tiddler.fields.text);
		for (var field in parsedTiddler) {
			if (formData !== "") {
				formData = formData + '&';
			}
			formData = formData + field + '=' + parsedTiddler[field];
		}
		var formRequest = new XMLHttpRequest();
		formRequest.open('POST', this.formURL, true);
		formRequest.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
		formRequest.onreadystatechange = function() {
			if (formRequest.readyState == 4 && formRequest.status == 200) {
				//Check if the request was successful and if we have a token.
				if (formRequest.responseText) {
					var response = JSON.parse(formRequest.responseText);
					if (response.success) {
						$tw.wiki.setText('Access Token', "text", undefined, response.token);
						$tw.wiki.setText('Access Token', "expires", undefined, response.expires);
						$tw.wiki.setText('Post Data', undefined, 'token', response.token);
					}
				}
			}
		}
		formRequest.onload = function() {
			console.log('here');
		}
		formRequest.send(formData);
	}


	return true; // Action was invoked
};

exports["action-post"] = ActionPost;

})();
