/*\
title: $:/plugins/OokTech/RandomValue/action-setrobbie.js
type: application/javascript
module-type: widget

Action widget to set a property is the status object for robbie.

<$action-setrobbie $key=KeyName $value=value/>

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var ActionSetRobbie = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
ActionSetRobbie.prototype = new Widget();

/*
Render this widget into the DOM
*/
ActionSetRobbie.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
ActionSetRobbie.prototype.execute = function() {
	this.mode = this.getAttribute('$mode', 'set');
	this.module = this.getAttribute('$attribute');
	this.key = this.getAttribute("$key");
	this.value = this.getAttribute("$value");
	this.name = this.getAttribute("$name");
	this.text = this.getAttribute("$text");
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
ActionSetRobbie.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(Object.keys(changedAttributes).length) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
ActionSetRobbie.prototype.invokeAction = function(triggeringWidget,event) {
	if (this.mode === 'set') {
    	var message = {"messageType": "SetValue", "key": this.key,"value": this.value};
		$tw.socket.send(JSON.stringify(message));
		console.log('Set Key Value');
	} else if (this.mode === 'get') {
		var message = {"messageType": "ModuleData", "name": this.module};
		$tw.socket.send(JSON.stringify(message));
		console.log('Get Key Value');
	} else if (this.mode === 'database') {
		var message = {"messageType": "DatabaseQuery"};
		$tw.socket.send(JSON.stringify(message));
		console.log('Database Query Sent');
	} else if (this.mode === 'Subscribe') {
		var message = {'messageType': 'Subscribe'};
		$tw.socket.send(JSON.stringify(message));
		console.log('Subscribed to Sensor Data');
	} else if (this.mode === 'Save') {
		var message = {'messageType': 'AddDatabaseEntry', 'name': this.name, 'text': this.text};
		$tw.socket.send(JSON.stringify(message));
	} else if (this.mode === 'Load') {
		var message = {'messageType': 'ReadDatabaseEntries'};
		$tw.socket.send(JSON.stringify(message));
	}
	return true; // Action was invoked
};

exports["action-setrobbie"] = ActionSetRobbie;

})();
