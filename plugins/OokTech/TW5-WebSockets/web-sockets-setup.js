/*\
title: $:/plugins/OokTech/WebSocketsInterface/web-sockets-setup.js
type: application/javascript
module-type: startup

Startup Actions Script thing

\*/
(function () {

	/*jslint node: true, browser: true */
	/*global $tw: false */
	"use strict";

	// Export name and synchronous status
	exports.name = "web-sockets-setup";
	exports.platforms = ["browser"];
	exports.after = ["render"];
	exports.synchronous = true;

	exports.startup = function() {
		// Do all actions on startup.
        function setup() {
			var IPTiddler = $tw.wiki.getTiddler("$:/ServerIP");
			var IPAddress = IPTiddler.fields.text;
			$tw.socket = new WebSocket(`ws://${IPAddress}:8000`);
            $tw.socket.onopen = openSocket;
            $tw.socket.onmessage = parseMessage;
            $tw.socket.binaryType = "arraybuffer";
        }

        var openSocket = function() {
			//This needs to fetch the current state of the robot and set the appropriate values in the wiki.
			//To do this it should fetch the components list, and then use that
			//to determine what other things it should fetch.
			updateComponents();
        }

        //function parseMessage(event) {
		var parseMessage = function(event) {
            console.log(event);
			var eventData = JSON.parse(event.data);
			if (eventData.messageType === "ComponentsList") {
				var creationFields = $tw.wiki.getCreationFields();
				var fields = {"title": "Components List"};
				var text = "{\n";
				Object.keys(eventData.data).forEach(function(key,value,array) {
					text += '\t"'+key+'"'+':'+'"'+eventData.data[key]+'"';
					text += (value < array.length-1)?',\n':'\n';
				});
				text += "}";
				fields.text = text;
				fields.type = 'application/json';
				$tw.wiki.addTiddler(new $tw.Tiddler(fields));
			} else if (eventData.messageType === "ModuleData") {
				$tw.wiki.setText(`Data - ${eventData.name}`, "text", undefined, JSON.stringify(eventData.data, null, 2));
			} else if (eventData.messageType === "DatabaseOutput") {
				$tw.wiki.setText('Database Output', "text", undefined, JSON.stringify(JSON.parse(event.data).data, null, 2));
			} else if (eventData.messageType === "Subscription") {
				console.log(JSON.parse(event.data).data.Accelerometer);
				console.log(JSON.parse(event.data).data.Thermometer);
				var data = JSON.parse(event.data).data;
				var tiddler = $tw.wiki.getTiddler('$:/RobbieState');
				var modificationFields = {'acc_x': data.Accelerometer.x, 'acc_y': data.Accelerometer.y, 'acc_z': data.Accelerometer.z, 'gyro_x': -1*Math.floor(data.Gyroscope.x/2), 'gyro_y': -1*Math.floor(data.Gyroscope.y/2), 'gyro_z': -1*Math.floor(data.Gyroscope.z/2), 'mag_x': data.Magnetometer.x, 'mag_y': data.Magnetometer.y, 'mag_z': data.Magnetometer.z, 'temp_f': data.Thermometer.Fahrenheit, 'temp_c': data.Thermometer.Celsius};
				$tw.wiki.addTiddler(new $tw.Tiddler(tiddler,modificationFields));
			}
        }

		function updateComponents () {
			var message = {"messageType": "RequestList"};
			$tw.socket.send(JSON.stringify(message));
		}

        //Send the message to node using the websocket

        //setTimeout(setup(), 20000);
		setup();
    }

})();
