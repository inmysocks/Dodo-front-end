/*\
title: $:/plugins/OokTech/action-get.js
type: application/javascript
module-type: widget

Action widget to send an XMLhttprequest with GET form data.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var ActionGet = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
ActionGet.prototype = new Widget();

/*
Render this widget into the DOM
*/
ActionGet.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
ActionGet.prototype.execute = function() {
	this.formURL = this.getAttribute("$url");
	this.formTiddler = this.getAttribute("$tiddler");
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
ActionGet.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(changedAttributes["$tiddler"] || changedAttributes["$url"]) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
ActionGet.prototype.invokeAction = function(triggeringWidget,event) {
	if (this.formTiddler && this.formURL) {
		var formRequest = new XMLHttpRequest();
		formRequest.open('GET', this.formURL, true);
		formRequest.onreadystatechange = function() {
			if (formRequest.readyState == 4 && formRequest.status == 200) {
				//Check if the request was successful and if we have a token.
				if (formRequest.responseText) {
					console.log(formRequest.responseText);
					var response = JSON.parse(formRequest.responseText);
					//Go through the contents of the response json and make
					//a tiddler for each response.
					console.log(response);

					response.forEach(function(post) {
						var tiddlerTitle = post.UserName + ' ' + post.PostID;
						$tw.wiki.setText(tiddlerTitle, 'text', undefined, post.PostText);
						$tw.wiki.setText(tiddlerTitle, 'poster', undefined, post.UserName);
						$tw.wiki.setText(tiddlerTitle, 'public', undefined, post.Public);
						$tw.wiki.setText(tiddlerTitle, 'conversation', undefined, post.Conversation);
						$tw.wiki.setText(tiddlerTitle, 'post_type', undefined, post.PostType);
						$tw.wiki.setText(tiddlerTitle, 'timestamp', undefined, post.TimeStamp);
					});
				}
			}
		}
		formRequest.onload = function() {
			console.log('here');
		}
		formRequest.send();
	}


	return true; // Action was invoked
};

exports["action-get"] = ActionGet;

})();
