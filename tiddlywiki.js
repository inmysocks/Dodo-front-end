#!/usr/bin/env node

/*

need to have sqlite installed on the system (sudo apt-get install sqlite3)

need to install ws: npm install ws
on my desktop I had to use: npm install ws@1
something about an error with my node version?
need to install express: npm install express
need to install sqlite: npm install sqlite
need to install node-ip: npm install ip

to run this type:

./start.sh

and then on any computer on the local network go to:

localip:8080

*/

var WebSocketServer = require('ws').Server;
var fs = require('fs');
var sqlite3 = require('sqlite3');

var SERVER_PORT = 8000;
var wss = new WebSocketServer({port: SERVER_PORT});
var connections = new Array;

wss.on('connection', handleConnection);

function handleConnection(client) {
    console.log("new connection");
    connections.push(client);
    client.on('message', function incoming(event) {
        self = this;
        if (typeof event === 'object') {
            console.log(Object.keys(event));
        }
        try {
            var eventData = JSON.parse(event);
            if (eventData.messageType === "AddDatabaseEntry") {
                $tw.db.run("CREATE TABLE IF NOT EXISTS Posts (PostID INTEGER PRIMARY KEY AUTOINCREMENT, UserName TEXT, PostText TEXT)");
                $tw.db.run("INSERT INTO Posts (UserName, PostText) VALUES ($name, $text)", {'$name': eventData.name, '$text': eventData.text});
            } else if (eventData.messageType === "ReadDatabaseEntries") {
                $tw.db.all("SELECT * FROM Posts", function(err, rows) {
                    self.send(JSON.stringify(Object.assign({},rows)));
                });
            }
        } catch (e) {

        }
    });
}

/*
This is invoked as a shell script by NPM when the `tiddlywiki` command is typed
*/

//We need the hostname of the computer running node so that other computres on the local network can properly respond to it.

var ip = require("ip");
var ipAddress = ip.address();

var fileData = `title: $:/ServerIP\n\n${ipAddress}`;
fs.writeFileSync('./editions/dodo-front-end/tiddlers/$__ServerIP.tid', fileData, function(err){if(err){console.log(err);}});

//Get the tiddlywiki object
var $tw = require("./boot/boot.js").TiddlyWiki();

$tw.db = new sqlite3.Database('./Post.db');

// Pass the command line arguments to the boot kernel
$tw.boot.argv = Array.prototype.slice.call(process.argv,2);

// Boot the TW5 app
$tw.boot.boot();
